import { Controller, Get } from '@nestjs/common';
import { RateLimit } from '../src';

@Controller('test')
export class TestController {
	@RateLimit(2000, 2)
	@Get('1')
	test1(): string {
		return 'OK';
	}

	@RateLimit(2000, 2, req => req.headers.rateId)
	@Get('2')
	test2(): string {
		return 'OK';
	}
}
