export type TokenProvider = (req: any) => string;

export interface RateLimitModuleOptions {
    enableForEnvironments?: string[],
    defaultTokenProvider?: TokenProvider;
    rateLimitMessage?: string;
}
