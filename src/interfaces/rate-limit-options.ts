import { TokenProvider } from './rate-limit-module-options';

export interface RateLimitOptions {
    points: number,
    duration: number,
    tokenProvider?: TokenProvider,
}
