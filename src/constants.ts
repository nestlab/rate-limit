import { TokenProvider } from './interfaces/rate-limit-module-options';

export const RATE_LIMIT_OPTIONS_REFLECT_KEY = 'RATE_LIMIT_OPTIONS_REFLECT_KEY';

export const RATE_LIMIT_MAP = Symbol('RATE_LIMIT_MAP');
export const RATE_LIMIT_MODULE_OPTIONS = Symbol('RATE_LIMIT_MODULE_OPTIONS');

export const DEFAULT_TOKEN_PROVIDER: TokenProvider = req => req.ip;
