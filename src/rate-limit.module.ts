import { DynamicModule, Module, Provider } from '@nestjs/common';
import { RateLimiterMemory } from 'rate-limiter-flexible';
import { RATE_LIMIT_MAP, RATE_LIMIT_MODULE_OPTIONS } from './constants';
import { RateLimitModuleOptions } from './interfaces/rate-limit-module-options';

@Module({})
export class RateLimitModule {
    static forRoot(options?: RateLimitModuleOptions): DynamicModule {
        const providers: Provider[] = [
            {
                provide: RATE_LIMIT_MAP,
                useValue: new Map<string, RateLimiterMemory>(),
            },
            {
                provide: RATE_LIMIT_MODULE_OPTIONS,
                useValue: options,
            },
        ];

        return {
            module: RateLimitModule,
            providers,
            exports: providers,
        }
    }
}
