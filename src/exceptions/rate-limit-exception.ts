import { ForbiddenException } from '@nestjs/common';

export class RateLimitException extends ForbiddenException {
    constructor(message?: string) {
        super(message);
    }
}
