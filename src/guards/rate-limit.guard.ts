import {
    applyDecorators,
    CanActivate,
    ExecutionContext,
    Inject,
    Injectable,
    SetMetadata,
    UseGuards,
} from '@nestjs/common';
import { RateLimiterMemory } from 'rate-limiter-flexible';
import { Request } from 'express';
import { RateLimitException } from '../exceptions/rate-limit-exception';
import { Reflector } from '@nestjs/core';
import {
    DEFAULT_TOKEN_PROVIDER,
    RATE_LIMIT_MAP,
    RATE_LIMIT_MODULE_OPTIONS,
    RATE_LIMIT_OPTIONS_REFLECT_KEY,
} from '../constants';
import { RateLimitModuleOptions, TokenProvider } from '../interfaces/rate-limit-module-options';
import { RateLimitOptions } from '../interfaces/rate-limit-options';

@Injectable()
export class RateLimitGuard implements CanActivate {
    constructor(@Inject(RATE_LIMIT_MAP) private readonly map: Map<string, RateLimiterMemory>,
                @Inject(RATE_LIMIT_MODULE_OPTIONS) private readonly moduleOptions: RateLimitModuleOptions,
                private readonly reflector: Reflector) {
    }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const environments = this.moduleOptions?.enableForEnvironments;

        if (environments && !environments.includes(process.env.NODE_ENV)) {
            return true;
        }

        const request: Request = context.switchToHttp().getRequest();

        let limiter: RateLimiterMemory = this.map.get(request.originalUrl);

        const options: RateLimitOptions = await this.reflector.get(RATE_LIMIT_OPTIONS_REFLECT_KEY, context.getHandler());

        if (!options) {
            return true;
        }

        if (!limiter) {
            limiter = new RateLimiterMemory({
                points: options.points,
                duration: options.duration / 1000,
            });
            this.map.set(request.originalUrl, limiter);
        }

        const tokenProvider = this.moduleOptions?.defaultTokenProvider || options.tokenProvider || DEFAULT_TOKEN_PROVIDER;

        const token = await tokenProvider(request);

        try {
            await limiter.consume(token, 1);

            return true;
        } catch (e) {
            throw new RateLimitException();
        }
    }
}

export const RateLimit = (ms: number, count = 1, tokenProvider?: TokenProvider) => applyDecorators(
    SetMetadata(RATE_LIMIT_OPTIONS_REFLECT_KEY, {
        points: count,
        duration: ms,
        tokenProvider,
    }),
    UseGuards(RateLimitGuard),
);
