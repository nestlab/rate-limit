# Rate limit module

Module for limiting the number of requests in a given time period.

### Install
```
$ npm i @nestlab/rate-limit
```

### Configuration

Example
```typescript
RateLimitModule.forRoot({
    // Optional. Default message for rate limit exception
    rateLimitMessage: 'You made a lot of requests. Try again later',

    // Optional. Default token provider function for identify requests
    defaultTokenProvider: req => req.ip,

    // Optional. Enable for environments 
    enableForEnvironments: ['production', 'staging']
});
```

### Usage

```typescript
@Controller()
export class AppController {
	@RateLimit(2000) // one request in 2 seconds for each IP address
	@Get('endpoint1')
	endpoint1(): string {
		return 'ENDPOINT 1 RESPONSE';
	}

	@RateLimit(10000, 5) // 5 requests in 10 seconds for each IP address
	@Get('endpoint2')
	endpoint2(): string {
		return 'ENDPOINT 2 RESPONSE';
	}

	@RateLimit(10000, 5, req => req.user.userId) // 5 requests in 10 seconds for each user
	@Get('endpoint3')
	endpoint3(): string {
		return 'ENDPOINT 3 RESPONSE';
	}
}
```

Enjoy!
